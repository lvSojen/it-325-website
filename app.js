//getElementById
var getTake1 = document.getElementById("spoiler-1")
var getTake2 = document.getElementById("spoiler-2")
// var getTake3 = document.getElementById("spoiler-3")

var backToTop = document.getElementById("back-to-top")

var toggleMyTake1 = document.getElementById("mytake-1")
var toggleMyTake2 = document.getElementById("mytake-2")
// var toggleMyTake3 = document.getElementById("mytake-3")

if (getTake1 != null) {
  getTake1.addEventListener("click", function () {
    if (toggleMyTake1.style.display === "none") {
      toggleMyTake1.style.display = "block"
      console.log("clicked")
    } else {
      toggleMyTake1.style.display = "none"
      console.log("clicked")
    }
  })

  getTake2.addEventListener("click", function () {
    if (toggleMyTake2.style.display === "none") {
      toggleMyTake2.style.display = "block"
    } else {
      toggleMyTake2.style.display = "none"
    }
  })
}

// getTake3.addEventListener("click", function () {
//   if (toggleMyTake3.style.display === "none") {
//     toggleMyTake3.style.display = "block"
//   } else {
//     toggleMyTake3.style.display = "none"
//   }
// })

backToTop.addEventListener("click", function () {
  document.body.scrollTop = 0
  document.documentElement.scrollTop = 0
  console.log("clicked")
})
